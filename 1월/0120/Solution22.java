package com.ssafy.algo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *  소금쟁이 중첩
 * @author leesohee
 *
 */
public class Solution22 {
	static int T, M, N;
	static int[][] visited;
	static int[][] striders;
	static int answer;
	static int[] dx = {0, -1, 1, 0, 0}; // {-, 상, 하, 좌, 우}
	static int[] dy = {0, 0, 0, -1, 1};
	
	public static void main(String[] args) throws IOException {
		System.setIn(new FileInputStream("input.txt")); // 파일 읽어오기 (파일 기본 위치는 프로젝트 폴더)
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		T = stoi(br.readLine());
		for (int i = 1; i <= T; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			M = stoi(st.nextToken()); // 연못의 크기
			N = stoi(st.nextToken()); // 소금쟁이의 수
			visited = new int[M][M];
			striders = new int[N][3];
			for (int j = 0; j < N; j++) {
				StringTokenizer st2 = new StringTokenizer(br.readLine());
				striders[j][0] = stoi(st2.nextToken()); // 행
				striders[j][1] = stoi(st2.nextToken()); // 열
				striders[j][2] = stoi(st2.nextToken()); // 방향
			}
			System.out.printf("#%d %d\n", i, move(striders));
		}
	}
	
	static int move(int[][] striders) {
		answer = N;
		for (int[] strider : striders) { // 한 마리씩 이동
			int x = strider[0];
			int y = strider[1];
			int dir = strider[2];
			if(visited[x][y] == 1) { // 처음 위치에 소금쟁이가 있으면
				answer--;
				continue;
			}
			for(int k =3; k >= 1; k--) {
				int nx = x + dx[dir] * k;
				int ny = y + dy[dir] * k;
				if(!isRange(nx, ny)) { // 연못을 벗어나면
					answer--;
					break;
				}
				if(visited[nx][ny] == 1) { // 이동한 곳에 소금쟁이가 있으면
					answer--;
					break;
				} else {
					x = nx; // 이동한 곳이 소금쟁이의 현재 위치가 된다. 
					y = ny;
				}
				if(k == 1) { // 마지막 이동이면
					visited[nx][ny] = 1; // 소금쟁이의 최종 위치가 된다. (생존)
				}
			}
		}
		return answer;
	}

	static boolean isRange(int x, int y) {
		return x<0 || x>=M || y<0 || y>=M ? false : true;
	}
	
	static int stoi(String string) {
		return Integer.valueOf(string);
	}
}
