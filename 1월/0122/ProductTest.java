package com.ssafy.hw05;

import java.util.Scanner;

public class ProductTest {
	ProductMgr manager = ProductMgr.getInstance();
	Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		ProductTest pt = new ProductTest();
		while (true) {
			int no = pt.menu();
			if (no == 6)
				break;
			switch (no) {
			case 1:
				pt.add();
				break;
			case 2:
				pt.list();
				break;
			case 3:
				pt.search();
				break;
			case 4:
				pt.delete();;
				break;
			case 5:
				pt.priceList();
				break;
			default:
				System.err.println("##정확한 번호를 입력하세요.");

			}
		}
	} // main

	public int menu() {
		System.out.println("==========재고 관리 메뉴=========");
		System.out.println("1. 상품 저장");
		System.out.println("2. 상품 조회(전체)");
		System.out.println("3. 특정 상품 조회");
		System.out.println("4. 특정 번호 삭제");
		System.out.println("5. 특정 가격 이하 검색");
		System.out.println("6. 종료");
		System.out.print("번호? ");
		return scan.nextInt();
	} // menu

	public void add() {
		System.out.println("상품 번호: ");
		int idx = scan.nextInt();
		System.out.println("상품 이름: ");
		String title = scan.next();
		System.out.println("가격: ");
		int price = scan.nextInt();
		System.out.println("수량 정보: ");
		int amount = scan.nextInt();

		Product book = new Product(idx, title, price, amount);

		manager.add(book);
	} // add

	public void list() {
		Product[] books = manager.list();
		for (Product book : books) {
			System.out.println(book.toString());
		}
	} // list
	
	
	public void search() {
		System.out.println("조회할 상품 번호: ");
		int idx = scan.nextInt();
		
		Product book = manager.list(idx);
		if(book == null) {
			System.err.println("##잘못된 번호 입력입니다!!");
		} else {
			System.out.println(book);
		}
	} // search
	
	public void delete() {
		System.out.println("삭제할 상품 번호: ");
		int idx = scan.nextInt();
		manager.delete(idx);
	} // delete

	private void priceList() {
		System.out.println("검색할 가격 정보: ");
		int price = scan.nextInt();
		
		Product[] products= manager.priceList(price);
		for (Product product : products) {
			System.out.println(product.toString());
		}
	}
}


