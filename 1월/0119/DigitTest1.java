package com.ssafy;

import java.io.IOException;
import java.util.Scanner;

public class DigitTest1 {
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
		int[] cnt_list = new int[100];
		
		while (true) {
			int i = sc.nextInt();
			if (i == 0) break;
			cnt_list[i / 10] += 1;
		} 	
		
		for (int j =0; j < cnt_list.length; j++) {
			if (cnt_list[j] > 0) {
				System.out.println(j+ " : " + cnt_list[j] + "개");
			}
		}
	}
}
