package com.ssafy.hw05;

public class ProductMgr {
	private static ProductMgr instance = new ProductMgr();

	private ProductMgr() {
	}

	private int MAX_SIZE = 100;
	private Product[] product = new Product[MAX_SIZE];
	private int size;
	
	public void add(Product book) {
		product[size++] = book;
	}

	public Product[] list() {
		Product[] p = new Product[size];
		for (int i = 0; i < p.length; i++) {
			p[i] = product[i];
		}
		return p;
	}

	public Product list(int num) {
		Product[] p = new Product[size];
		
		for (int i = 0; i < p.length; i++) {
			if (product[i].getIdx() == num) {
				return product[i];
			}
		}
		return null; // 일치하는 상품을 찾지 못했을 때
	}

	public void delete(int num) {
		for (int i = 0; i < product.length; i++) {
			if (product[i].getIdx() == num) {
				product[i] = product[size - 1];
				size--;
				System.out.println("삭제 완료");
				break;
			}
		}
	}

	public Product[] priceList(int price) {
		int count = size;
		int idx = 0;
		Product[] p = new Product[count];
		for (int i = 0; i < size; i++) {
			if (product[i].getPrice() <= price) {
				p[idx++] = product[i];
			} else {
				count--;
			}
		}
		Product[] resizedP = new Product[count];
		for (int i =0; i< count ;i ++) {
			resizedP[i] = p[i];
		}
		return resizedP;
	}

	public static ProductMgr getInstance() {
		return instance;
	}

}
