package com.ssafy.hw05;

public class Product {
	private int idx;
	private String title;
	private int price;
	private int	amount;

	public Product() {
		// TODO Auto-generated constructor stub
	}



	public Product(int idx, String title, int price, int amount) {
		super();
		this.idx = idx;
		this.title = title;
		this.price = price;
		this.amount = amount;
	}

	

	public int getIdx() {
		return idx;
	}



	public void setIdx(int idx) {
		this.idx = idx;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public int getPrice() {
		return price;
	}



	public void setPrice(int price) {
		this.price = price;
	}



	public int getAmount() {
		return amount;
	}



	public void setAmount(int amount) {
		this.amount = amount;
	}



	@Override
	public String toString() {
		return idx + "\t| " + title + "\t| " + price + "\t| " + amount + "\t| ";
	}

}
