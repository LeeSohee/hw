package com.ssafy;

public class DigitTest1 {
	public static void main(String[] args) {
		int num = 1;
		for ( int i = 5 ; i > 0 ; i-- ){
			for( int j = 0 ; j < 5-i ; j++ ) {
				System.out.print("   ");
			}
			for( int k = 1 ; k <= i ; k++ ){
				System.out.printf("%3d",num++);
			}
			System.out.println('\n');
		}
	}
}
