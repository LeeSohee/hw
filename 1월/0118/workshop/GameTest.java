package com.ssafy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GameTest {
	static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	static int Game() throws IOException {
		// 1: 짐
		// 2: 이김 
		// 3: 비김
		System.out.print("가위바위보 중 하나 입력: ");
		String finger = br.readLine();
		int comp_finger = (int) (Math.random() * 3) + 1;
		switch(finger) {
			case "가위":
				if(comp_finger == 2) return 2;
				else if(comp_finger == 3) return 1;
				break;
			case "바위":
				if(comp_finger == 3) return 2;
				else if(comp_finger == 1) return 1;
				break;
			case "보":
				if(comp_finger == 1) return 2;
				else if(comp_finger == 2) return 1;
				break;
			default:
				break;
		}
		return 3;
	}
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		System.out.println("가위바위보 게임을 시작합니다. 아래 보기 중 하나를 고르세요.");
		System.out.println("1.5판 3승 \n2.3판 2승 \n3.1판 1승");
		System.out.print("번호를 입력하세요: ");
		int num = Integer.parseInt(br.readLine());
		
		int comp_win = 0; // 컴퓨터가 이긴 횟수
		int user_win = 0; // 사용자가 이긴 횟수 
		int break_num = 0; 
		boolean isOneRound = false;
		switch(num) {
			case 1: // 5판 3승
				break_num = 3;
				break; 
			case 2: // 3판 2승 
				break_num = 2;
				break;
			case 3: // 1판 1승
				break_num = 1;
				isOneRound = true;
				break;
			default:
				break;
		}
		
		
		while(comp_win < break_num && user_win < break_num) {
			int result = Game();
			switch (result) {
				case 1:
					System.out.println("졌습니다."); // 컴퓨터가 이김
					comp_win++;
					break;
				case 2:
					System.out.println("이겼습니다."); // 사용자가 이김
					user_win++;
					break;
				case 3:
					System.out.println("비겼습니다.");
					if(isOneRound) {
						comp_win++; user_win++;
					}
					break;
				default:
					break;
			}
		}
		
		if(comp_win > user_win) {
			System.out.println("### 컴퓨터 승!!!");
		} else if (comp_win == user_win) {
			System.out.println("### 무승부!!!");
		} else {
			System.out.println("### 사용자 승!!!");
		}
	}
}
