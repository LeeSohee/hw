package com.ssafy;

public class DigitTest2 {
	public static void main(String[] args) {
		int num = 1;
		int num2 = 5;
		for(int i = 3; i > 0; i--){
			for(int j = 0; j < 3-i; j++) {
				System.out.print("   ");
			}
			
			for(int k = 0; k < num2; k++){
				System.out.printf("%3d",num++);
			}
			num2-=2;
			System.out.println('\n');
		}
		
		num2 = 3;
		for ( int i = 2 ; i > 0 ; i-- ){
			for(int j = 3-i; j < 2; j++ ) {
				System.out.print("   ");
			}
			
			for( int k = 0 ; k < num2  ; k++){
				System.out.printf("%3d",num++);
			}
			num2+=2;
			System.out.println('\n');
		}
	}
}

