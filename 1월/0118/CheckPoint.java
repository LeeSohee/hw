package com.ssafy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CheckPoint {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] str = br.readLine().split(" ");
		int height = Integer.parseInt(str[0]);
		int	weight = Integer.parseInt(str[1]);
		int per = weight+100-height;
		
		System.out.printf("비만수치는 %d입니다.", per);
		System.out.println();
		if(per > 0) {
			System.out.println("당신은 비만이군요");
		}
	}
}
