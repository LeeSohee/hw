package com.ssafy;

public class CircleArea {
	public static void main(String[] args){
		final double PI = 3.1415;
		double Area = 5 * 5 * PI;
		System.out.printf("반지름이 5Cm인 원의 넓이는 %.1fCm2 입니다.", Area);
	}
}
