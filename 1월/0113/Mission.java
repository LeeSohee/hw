package com.ssafy;

public class Mission {
	public static void main(String[] args) {
		System.out.println("문제 1");
		for(int i =1; i < 6; i++) System.out.println(i);
		System.out.println("문제 2");
		for(int i =1; i < 6; i++) System.out.print(i+ " ");
		System.out.println("\n문제 3");
		for(int i =5; i > 0; i--) System.out.print(i+ " ");
		System.out.println("\n문제 4");
		for(int i =3; i < 15; i *= 3) System.out.print(i+ " ");
		System.out.println("\n문제 5");
		System.out.print("[");
		for(int i = 5; i < 25; i+=5) {
			if(i < 20) { System.out.print(i+ ", "); }
			else { System.out.print(i); }
		}
		System.out.print("]");
	
	}
}